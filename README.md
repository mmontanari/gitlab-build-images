[![pipeline status](https://gitlab.com/mmontanari/gitlab-build-images/badges/master/pipeline.svg)](https://gitlab.com/mmontanari/gitlab-build-images/-/commits/master)

# Building CI images

This project generates and pushes to Docker Hub registry.

# How to generate a new image

Add a new directory with a Dockerfile containing the instructions for the
image.

Then edit .gitlab-ci.yml to add the build instructions, commit and push.

# How to run an image locally to reproduce bugs

If you work on a private registry, firstly you need to login:

    docker login  -u mmontanari

To ensure you have access to the latest container, pull it from the registry:

    docker pull mmontanari/devenv:website-hugo-builder

An finally run and enter an interactive section:

    docker run -it mmontanari/devenv:website-hugo-builder
