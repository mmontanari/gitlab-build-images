#!/bin/sh

# Parse inputs
image=$1
startdir=$2
registry=$3
project=$4

# Build image
set -e


docker logout
echo "$DOCKER_PASS" | docker login --username mmontanari --password-stdin docker.io

docker build --no-cache -t docker.io/mmontanari/devenv:$image $startdir
docker push docker.io/mmontanari/devenv:$image

exit 0
